
# evgo-dev-api

### Dependencies

After checking out this repo, run `npm install` to install dependencies

### Running in Dev Mode

Run `npm start` to launch the GraphQL development webserver at `http://localhost:4000`

