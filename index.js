const { ApolloServer, gql } = require("apollo-server");
const fetch = require("node-fetch");
// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`
  type Mission {
    mission_name: String
    mission_id: String
    description: String
  }

  type Rocket {
    rocket_name: String
    rocket_id: String
    rocket_type: String
  }
  type Links {
    video_link: String
    youtube_id: String
    mission_patch_small: String
  }
  type Launch {
    flight_number: Int
    mission_name: String
    mission_id: [String]
    launch_year: String
    launch_date_unix: Int
    rocket: Rocket
    links: Links
    details: String
  }

  type Query {
    launches: [Launch]
  }
`;

const resolvers = {
  Query: {
    launches: () =>
      fetch("https://api.spacexdata.com/v3/launches/").then((res) =>
        res.json()
      ),
  },
};

const server = new ApolloServer({ typeDefs, resolvers });
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
